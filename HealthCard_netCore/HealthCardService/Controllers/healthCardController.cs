﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HealthCardService.Common;
using HealthCardService.Model;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using NLog;

namespace HealthCardService.Controllers
{
    [Route("api/[controller]")]
    public class healthCardController : Controller
    {
        HealthCardContext healthCardContext = new HealthCardContext();

        // 根据二维码远程获取电子健康卡信息
        [HttpPost("getHealthCardInfoByQrcode")]
        public async Task<IActionResult> getHealthCardInfoByQrcode([FromBody] JObject paras)
        {
            // 1.解析传入参数,获取qrCodeText,构造req参数。
            var qrCodeText = paras["qrCodeText"].ToString();    //解析入参
            IDictionary<string, Object> req = new Dictionary<string, Object>
            {
                { "qrCodeText", qrCodeText }
            };
            // 2.调用通用请求接口，获取返回数据
            var _result = genericCallInterface.CallInterface(AppSettings.AppSetting("BASE_URL_OPEN"), AppSettings.AppSetting("URL_GetCardByQRCode"), req);

            return Ok(_result);

        }

        // 获取电子健康卡列表
        [HttpPost("getHealthCardList")]
        public async Task<IActionResult> getHealthCardList([FromBody] JObject paras)
        {

            // 1.解析入参，获取openId。
            // 2.根据openId，读取数据库记录。
            var openId = paras["openId"].ToString();
            var cardList = healthCardContext.HealthCardInfo.Where(c => c.OpenId == openId).ToList();

            IDictionary<string, object> _result = new Dictionary<string, object>
            {
                { "retcode", 0 },
                { "retmsg", "success" },
                { "cardcount", cardList.Count },
                { "cardList", cardList }
            };

            return Ok(_result);

        }

        // 注册电子健康卡
        [HttpPost("registHealthCard")]
        public async Task<IActionResult> registHealthCard([FromBody] JObject paras)
        {
            // 1.解析入参，获取提交的各项注册信息,根据注册信息，构造req参数
            string wechatCode = paras["wechatCode"].ToString();
            string name = paras["name"].ToString();
            string gender = paras["gender"].ToString();
            string nation = paras["nation"].ToString();
            string birthday = paras["birthday"].ToString();
            string idNumber = paras["idNumber"].ToString();
            string idType = paras["idType"].ToString();
            string address = paras["address"].ToString();
            string phone1 = paras["phone1"].ToString();
            string phone2 = paras["phone2"].ToString();
            string patid = paras["patid"].ToString();
            IDictionary<string, Object> req = new Dictionary<string, Object>
            {
                { "wechatCode", wechatCode },
                { "name", name },
                { "gender", gender },
                { "nation", nation },
                { "birthday", birthday },
                { "idNumber", idNumber },
                { "idType", idType },
                { "address", address },
                { "phone1", phone1 },
                { "phone2", phone2 },
                { "patid", patid }
            };
            // 2.调用通用请求接口，获取返回数据
            var _result = genericCallInterface.CallInterface(AppSettings.AppSetting("BASE_URL_OPEN"), AppSettings.AppSetting("URL_RegistHealthCard"), req);

            return Ok(_result);

        }

        //通过健康卡授权码获取健康卡
        [HttpPost("getHealthCardByHealthCode")]
        public async Task<IActionResult> getHealthCardByHealthCode([FromBody] JObject paras)
        {

            // 1.解析入参，获取healthCode，构造req参数
            string healthCode = paras["healthCode"].ToString();
            IDictionary<string, Object> req = new Dictionary<string, Object>
            {
                { "healthCode", healthCode }
            };
            // 2.调用通用请求接口，获取返回数据
            var _result = genericCallInterface.CallInterface(AppSettings.AppSetting("BASE_URL_OPEN"), AppSettings.AppSetting("URL_GetHealthCardByHealthCode"), req);

            return Ok(_result);

        }

        //OCR接口
        [HttpPost("ocrInfo")]
        public async Task<IActionResult> ocrInfo([FromBody] JObject paras)
        {
            // 1.解析入参，获取imageContent，构造req参数
            string imageContent = paras["imageContent"].ToString();
            IDictionary<string, Object> req = new Dictionary<string, Object>
            {
                { "imageContent", imageContent }
            };
            // 2.调用通用请求接口，获取返回数据
            var _result = genericCallInterface.CallInterface(AppSettings.AppSetting("BASE_URL_OPEN"), AppSettings.AppSetting("URL_OCRInfo"), req);

            return Ok(_result);
        }

        //绑定健康卡和院内ID关系接口
        [HttpPost("bindCardRelation")]
        public async Task<IActionResult> bindCardRelation([FromBody] JObject paras)
        {
            // 1.解析入参，获取patid,qrCodetext，构造req参数
            string patid = paras["patid"].ToString();
            string qrCodeText = paras["qrCodeText"].ToString();
            IDictionary<string, Object> req = new Dictionary<string, Object>
            {
                { "patid", patid },
                { "qrCodeText", qrCodeText }
            };
            // 2.调用通用请求接口，获取返回数据
            var _result = genericCallInterface.CallInterface(AppSettings.AppSetting("BASE_URL_OPEN"), AppSettings.AppSetting("URL_BindCardRelation"), req);

            return Ok(_result);
        }

        //用卡数据监测接口
        [HttpPost("reportHISData")]
        public async Task<IActionResult> reportHISData([FromBody] JObject paras)
        {
            // 1.解析入参，获取参数列表，构造req参数
            string qrCodeText = paras["qrCodeText"].ToString();
            string idCardNumber = paras["idCardNumber"].ToString();
            string name = paras["name"].ToString();
            string time = paras["time"].ToString();
            string hospitalId = paras["hospitalId"].ToString();
            string scene = paras["scene"].ToString();
            string department = paras["department"].ToString();
            string cardType = paras["cardType"].ToString();
            string cardChannel = paras["cardChannel"].ToString();
            string cardCostTypes = paras["cardCostTypes"].ToString();
            IDictionary<string, Object> req = new Dictionary<string, Object>
            {
                { "qrCodeText", qrCodeText },
                { "idCardNumber", idCardNumber },
                { "name", name },
                { "time", time },
                { "hospitalId", hospitalId },
                { "scene", scene },
                { "department", department },
                { "cardType", cardType },
                { "cardChannel", cardChannel },
                { "cardCostTypes", cardCostTypes }
            };
            // 2.调用通用请求接口，获取返回数据
            var _result = genericCallInterface.CallInterface(AppSettings.AppSetting("BASE_URL_OPEN"), AppSettings.AppSetting("URL_ReportHISData"), req);

            return Ok(_result);
        }

        //获取卡包订单ID接口
        [HttpPost("getOrderIdByOutAppId")]
        public async Task<IActionResult> getOrderIdByOutAppId([FromBody] JObject paras)
        {
            // 1.解析入参，获取qrCodeText，从AppSetting中获取appId，构造req参数
            string appId = AppSettings.AppSetting("appId");
            string qrCodeText = paras["qrCodeText"].ToString();
            IDictionary<string, Object> req = new Dictionary<string, Object>
            {
                { "appId", appId },
                { "qrCodeText", qrCodeText }
            };
            // 2.调用通用请求接口，获取返回数据
            var _result = genericCallInterface.CallInterface(AppSettings.AppSetting("BASE_URL_OPEN"), AppSettings.AppSetting("URL_GetOrderIdByOutAppId"), req);

            return Ok(_result);
        }
    }
}
