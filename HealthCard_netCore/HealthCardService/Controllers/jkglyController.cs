﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HealthCardService.Model;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace HealthCardService.Controllers
{
    //[Route("api/[controller]")]
    [Route("api/jkgly")]
    [ApiController]
    public class jkglyController : ControllerBase
    {
        HealthCardContext healthCardContext = new HealthCardContext();
        private readonly ILogger iLogger = LogManager.GetCurrentClassLogger();

        /**
         * 接口：用卡数据监测接口
         */
        //public  async Task<IActionResult>  reportHISData(ReportHISData reportHISData)
        //{

        //    //调用接口
        //    //CommonIn commonIn = InitCommonIn.getCommonIn(getApptoken());
        //    //JSONObject reportHISDataRsp = InitCommonIn.getHealthCard().reportHISData(commonIn, reportHISData);
        //    ////打印响应
        //    //System.out.println(JSON.toJSONString(reportHISDataRsp));
        //    //return reportHISDataRsp;
        //    return null;
        //}


        [HttpGet("getHealthCardByCode/{code_text}")]
        public async Task<IActionResult> GetHealthCardByCode(string code_text)
        {
            var person = healthCardContext.HealthCardInfo.Where(c => c.QrCodeText == code_text).FirstOrDefault();
            if (person == null)
            {
                return NotFound();
            }
            iLogger.Info("测试logstash日志请求1");
            return Ok(person);
        }

        #region MyRegion


        //**
        // * 注册电子健康卡
        // * @param healthCardInfoReq
        // * @return
        // */
        //public JSONObject getHeathCard(HealthCardInfo healthCardInfoReq){
        //    /**
        //     * 接口：注册健康卡接口
        //     */
        //    Integer healthCards = healthCardService.findHealthCardInfoByIdNumber(healthCardInfoReq.getIdNumber(),healthCardInfoReq.getOpenId());
        //    JSONObject result= new JSONObject();
        //    if(healthCards==0){
        //        CommonIn commonIn= InitCommonIn.getCommonIn(getApptoken());
        //        String idCard = healthCardInfoReq.getIdNumber();
        //        int i =Integer.valueOf(idCard.substring(16,17));
        //        if(i%2==1){
        //            healthCardInfoReq.setGender("男");
        //        } else {
        //            healthCardInfoReq.setGender("女");
        //        }
        //        String  year =idCard.substring(6,10);
        //        String month =idCard.substring(10,12);
        //        String day =idCard.substring(12,14);
        //        healthCardInfoReq.setBirthday(year+"-"+month+"-"+day);
        //        healthCardInfoReq.setIdType("01");
        //        JSONObject healthCardInfoRsp=InitCommonIn.getHealthCard().registerHealthCard(commonIn,healthCardInfoReq);
        //        //打印响应

        //        result.put("retcode",healthCardInfoRsp.getJSONObject("commonOut").getIntValue("resultCode"));
        //        result.put("retmsg",healthCardInfoRsp.getJSONObject("commonOut").getString("errMsg"));
        //        System.out.println(JSON.toJSONString(healthCardInfoRsp));
        //        HealthCardInfoEntity healthCardInfoEntity=new HealthCardInfoEntity();
        //        JSONObject rsp =healthCardInfoRsp.getJSONObject("rsp");
        //        String qrCodeText =rsp.getString("qrCodeText");
        //        String healthCardId =rsp.getString("healthCardId");
        //        String adminExt =rsp.getString("adminExt");
        //        System.out.println(adminExt);
        //        healthCardInfoReq.setQrCodeText(qrCodeText);
        //        healthCardInfoReq.setOpenId(healthCardInfoReq.getOpenId());
        //        healthCardInfoReq.setHealthCardId(healthCardId);
        //        healthCardService.insertHealthCardInfo(healthCardInfoReq);

        //    }else {
        //        result.put("retcode",0);
        //        result.put("retmsg","此号码您已注册，不可重复注册");
        //    }

        //    return result;
        //}


        ///**
        // * 通过健康卡授权码获取健康卡信息
        // * @param healthCode
        // * @return
        // */
        //public JSONObject getHealthCardByHealthCode(String healthCode){
        //    HealthCardServerImpl healthCard = InitCommonIn.getHealthCard();
        //    //调用接口
        //    JSONObject healthCardInfoRsp=healthCard.getHealthCardByHealthCode(InitCommonIn.getCommonIn(getApptoken()),healthCode);
        //    return healthCardInfoRsp;
        //}


        //public String getApptoken(){
        //    String apptoken1;
        //    Long expiresIn=healthCardService.findHealthCardExpiresIn();
        //    Long dateTime=System.currentTimeMillis();
        //    if(dateTime-expiresIn>3600000||dateTime-expiresIn<0){
        //        apptoken1=InitCommonIn.getApptoken();
        //        Long expiresIn2=System.currentTimeMillis();
        //        healthCardService.updateHealthCardApptoken(apptoken1,expiresIn2);

        //    }else{
        //        apptoken1=healthCardService.findHealthCardApptoken();
        //    }

        //    return apptoken1;
        //}

        ///**
        // * 接口：获取卡包订单ID接口
        // */
        //public String getOrderId(String qrCodeText) {

        //    //调用接口

        //    CommonIn commonIn = InitCommonIn.getCommonIn(getApptoken());
        //    JSONObject orderIdObj=InitCommonIn.getHealthCard().getOrderIdByOutAppId(commonIn,InitCommonIn.appId,qrCodeText);
        //    //打印响应
        //    String orderId=orderIdObj.getJSONObject("rsp").getString("orderId");
        //    System.out.println(JSON.toJSONString(orderId));
        //    return orderId;

        //}
        ///**
        // * 接口：OCR接口
        // */
        //public JSONObject registerByOcr(String imageContent){
        //    //调用接口
        //    CommonIn commonIn = InitCommonIn.getCommonIn(getApptoken());
        //    HealthCardServerImpl  healthCard=InitCommonIn.getHealthCard();
        //    JSONObject idCardInfo=healthCard.ocrInfo(commonIn,imageContent);
        //    //打印响应
        //    System.out.println(JSON.toJSONString(idCardInfo));
        //    /*if(idCardInfo.getJSONObject("commonOut").getIntValue("resultCode")==0){
        //        HealthCardInfo healthCardInfo=new HealthCardInfo();
        //        JSONObject cardInfo=idCardInfo.getJSONObject("rsp").getJSONObject("cardInfo");
        //        healthCardInfo.setName(cardInfo.getString("name"));
        //        healthCardInfo.setIdNumber(cardInfo.getString("id"));
        //        healthCardInfo.setName(cardInfo.getString("name"));
        //        healthCardInfo.setGender(cardInfo.getString("gender"));
        //        healthCardInfo.setNation(cardInfo.getString("nation"));
        //        healthCardInfo.setBirthday(cardInfo.getString("birth").replace("/","-"));
        //        healthCardInfo.setAddress(cardInfo.getString("address"));
        //        healthCardInfo.setName(cardInfo.getString("name"));
        //        return
        //    }else {
        //        return idCardInfo;
        //    }*/
        //    return idCardInfo;

        //}
        #endregion
        #region Demo    

        //public async Task<IActionResult> TestExcept(string qrcodetext)
        //{
        //    try
        //    {
        //        throw new Exception("A test except");
        //    }
        //    catch (Exception ex)
        //    {
        //        return StatusCode(500, "An Error Occurred");
        //    }
        //}

        //// GET api/values
        //[HttpGet]
        //public ActionResult<IEnumerable<string>> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/values/5
        //[HttpGet("{id}")]
        //public ActionResult<string> Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/values
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT api/values/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE api/values/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
        #endregion
    }
}

