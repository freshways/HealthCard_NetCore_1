﻿using System;
using System.Collections.Generic;
using System.Linq;
using HealthCardService.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace HealthCardService.Common
{
    public class AppTokenHelper
    {

        //调用获取Token接口，获取新Token.
        public static string getToken()
        {
            using (var context = new HealthCardContext())
            {
                var appToken = context.HealthCardApptoken.FirstOrDefault();
                //判断Token是否过期
                if (appToken.ExpiresIn < COMMON.ConvertToTimeStamp(DateTime.Now) + 60)
                {
                    //重新获取Token
                    // 1.构造req参数
                    IDictionary<string, Object> req = new Dictionary<string, Object>
                    {
                        { "appId", AppSettings.AppSetting("appId") }
                    };

                    // 2.调用通用请求接口，获取返回值，解析。将Token和失效时间回写数据，并返回Token
                    var _result = genericCallInterface.CallInterface(AppSettings.AppSetting("BASE_URL_OPEN"), AppSettings.AppSetting("URL_GetToken"), req);

                    //此处判断待修正：最好的判断条件为resultCode==0
                    if (_result["commonOut"]["resultCode"].ToString() == "0")
                    {
                        //请求成功,解析返回值
                        string token = _result["rsp"]["appToken"].ToString();
                        long expires = Convert.ToInt64(_result["rsp"]["expiresIn"].ToString());
                        //将结果中的Token和失效期回写数据库
                        appToken.AppToken = token;
                        appToken.ExpiresIn = COMMON.ConvertToTimeStamp(DateTime.Now) + expires;
                        context.HealthCardApptoken.Update(appToken);
                        context.SaveChanges();

                        return token;
                    }
                    else
                    {
                        //请求失败，返回错误值。
                        return "";
                    }
                }
                else
                {
                    //未过期，直接返回数据中的token
                    return appToken.AppToken;
                }
            }
        }
    }
}
