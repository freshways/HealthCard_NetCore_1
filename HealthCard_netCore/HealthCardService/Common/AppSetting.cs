﻿using Microsoft.Extensions.Configuration;

namespace HealthCardService.Common
{
    public class AppSettings
    {
        private static IConfigurationSection appSections;

        public static string AppSetting(string key)
        {
            string str = "";
            if (appSections.GetSection(key) != null)
            {
                str = appSections.GetSection(key).Value;
            }
            return str;
        }

        public static void SetAppSetting(IConfigurationSection section)
        {
            appSections = section;
        }
    }
}
