﻿using System;
using System.Collections.Generic;

namespace HealthCardService.Common
{
    public class reqHelper
    {
        private IDictionary<string, object> para;

        //构造通用req参数
        /*  复杂参数
            SortedDictionary<string,Object> cardInfo = new SortedDictionary<string,Object>();
            cardInfo.Add("name","张三");
            cardInfo.Add("idCard","430203188808084321");
            cardInfo.Add("gender","男");
            cardInfo.Add("nation","汉族");
            cardInfo.Add("birthday", "1999-03-23");
            cardInfo.Add("address", "腾讯大厦"); 
            req.Add("cardInfo", cardInfo);
        */
        public reqHelper(string paraName, SortedDictionary<string, Object> paras)
        {
            para = new Dictionary<string, Object>
            {
                { paraName, paras }
            };
        }

        public IDictionary<string, object> Req { get => para; set => para = value; }
    }
}
