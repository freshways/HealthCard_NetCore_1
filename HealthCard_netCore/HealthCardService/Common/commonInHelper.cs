﻿using System;
using System.Collections.Generic;

namespace HealthCardService.Common
{
    public class commonInHelper
    {
        private IDictionary<string, object> para;

        //构造通用commonIn参数
        public commonInHelper(Boolean withToken = true)
        {
            para = new Dictionary<string, object>
            {
                { "appToken", withToken ? AppTokenHelper.getToken() : "" },
                { "requestId", Guid.NewGuid().ToString("N") },
                { "hospitalId", AppSettings.AppSetting("hospitalId") },
                { "timestamp", COMMON.ConvertToTimeStamp(DateTime.Now) },
                { "channelNum", 0 }
            };
        }

        //构造签名后commonIn参数
        public void CommonInSign(IDictionary<string, object> req)
        {
            string sign = SignHelper.getSign(para, req);
            para.Add("sign", sign);
        }

        public IDictionary<string, object> CommonIn { get => para; set => para = value; }
    }
}
