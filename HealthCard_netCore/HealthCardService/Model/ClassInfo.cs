﻿using System;
using System.Collections.Generic;

namespace HealthCardService.Model
{
    public partial class ClassInfo
    {
        public ClassInfo()
        {
            Student = new HashSet<Student>();
        }

        public int Id { get; set; }
        public string ClassName { get; set; }
        public string Test { get; set; }
        public string 汉字 { get; set; }

        public virtual ICollection<Student> Student { get; set; }
    }
}
