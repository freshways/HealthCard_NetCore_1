﻿using System;
using System.Collections.Generic;

namespace HealthCardService.Model
{
    public partial class UseCardChannelChild
    {
        public string ParentChannelCode { get; set; }
        public string ChildChannelCode { get; set; }
        public string ChildChannelName { get; set; }
        public Guid Uuid { get; set; }
    }
}
