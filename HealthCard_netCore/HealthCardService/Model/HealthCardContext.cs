﻿using HealthCardService.Common;
using Microsoft.EntityFrameworkCore;

namespace HealthCardService.Model
{
    public partial class HealthCardContext : DbContext
    {
        public HealthCardContext()
        {
        }

        public HealthCardContext(DbContextOptions<HealthCardContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ClassInfo> ClassInfo { get; set; }
        public virtual DbSet<GobalResCode> GobalResCode { get; set; }
        public virtual DbSet<HealthCardApptoken> HealthCardApptoken { get; set; }
        public virtual DbSet<HealthCardHospital> HealthCardHospital { get; set; }
        public virtual DbSet<HealthCardHospitalList> HealthCardHospitalList { get; set; }
        public virtual DbSet<HealthCardInfo> HealthCardInfo { get; set; }
        public virtual DbSet<HealthCardTemp> HealthCardTemp { get; set; }
        public virtual DbSet<Student> Student { get; set; }
        public virtual DbSet<UseCardChannelChild> UseCardChannelChild { get; set; }
        public virtual DbSet<UseCardChannelParent> UseCardChannelParent { get; set; }

        // Unable to generate entity type for table 'dbo.useCardLink_parent'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.identityCardType'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.useCardChargType_copy1'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.useCardDepartment_parent'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.全局返回码_copy1'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.用卡渠道_copy1'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.用卡环节_copy1'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.用卡科室_copy1'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.useCardChargType'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.证件类型_copy1'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.useCardLink_child'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.useCardDepartment_child'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(AppSettings.AppSetting("ConnString"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<ClassInfo>(entity =>
            {
                entity.HasIndex(e => e.Id)
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClassName).HasMaxLength(15);

                entity.Property(e => e.Test)
                    .HasColumnName("test")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.汉字)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GobalResCode>(entity =>
            {
                entity.HasKey(e => e.Uuid)
                    .HasName("PK__全局返回码__65A475E7A64CA58E");

                entity.ToTable("gobalResCode");

                entity.Property(e => e.Uuid)
                    .HasColumnName("UUID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ResCode)
                    .HasColumnName("resCode")
                    .HasMaxLength(20);

                entity.Property(e => e.ResDetail)
                    .HasColumnName("resDetail")
                    .HasMaxLength(255);

                entity.Property(e => e.ResMsg)
                    .HasColumnName("resMsg")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<HealthCardApptoken>(entity =>
            {
                entity.ToTable("healthCard_apptoken");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.AppId)
                    .HasColumnName("appId")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AppSecret)
                    .HasColumnName("appSecret")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AppToken)
                    .HasColumnName("appToken")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ExpiresIn).HasColumnName("expiresIn");
            });

            modelBuilder.Entity<HealthCardHospital>(entity =>
            {
                entity.ToTable("healthCard_hospital");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.HealthCardId)
                    .HasColumnName("healthCardId")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.HospitalCode)
                    .HasColumnName("hospitalCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HospitalNumber)
                    .HasColumnName("hospitalNumber")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<HealthCardHospitalList>(entity =>
            {
                entity.ToTable("healthCard_hospitalList");

                entity.Property(e => e.AppTokenId).HasColumnName("appTokenId");

                entity.Property(e => e.HospitalId).HasColumnName("hospitalId");
            });

            modelBuilder.Entity<HealthCardInfo>(entity =>
            {
                entity.ToTable("healthCard_info");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Address)
                    .HasColumnName("address")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Birthday)
                    .HasColumnName("birthday")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .HasColumnName("gender")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.HealthCardId)
                    .HasColumnName("healthCardId")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IdNumber)
                    .HasColumnName("idNumber")
                    .HasMaxLength(18)
                    .IsUnicode(false);

                entity.Property(e => e.IdType)
                    .HasColumnName("idType")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nation)
                    .HasColumnName("nation")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OpenId)
                    .HasColumnName("openId")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Patid)
                    .HasColumnName("patid")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phid)
                    .HasColumnName("phid")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone1)
                    .HasColumnName("phone1")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Phone2)
                    .HasColumnName("phone2")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.QrCodeText)
                    .HasColumnName("qrCodeText")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<HealthCardTemp>(entity =>
            {
                entity.HasKey(e => e.OpenId)
                    .HasName("PK__HealthCa__72E12F1A7849DB76");

                entity.ToTable("HealthCard_temp");

                entity.Property(e => e.OpenId)
                    .HasColumnName("openId")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.HealthCode)
                    .HasColumnName("healthCode")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdNumber)
                    .HasColumnName("idNumber")
                    .HasMaxLength(18)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nation)
                    .HasColumnName("nation")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WechatCode)
                    .HasColumnName("wechatCode")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Student>(entity =>
            {
                entity.HasIndex(e => e.ClassInfoId);

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClassInfoId).HasColumnName("ClassInfoID");

                entity.Property(e => e.StudentName).HasMaxLength(10);

                entity.HasOne(d => d.ClassInfo)
                    .WithMany(p => p.Student)
                    .HasForeignKey(d => d.ClassInfoId);
            });

            modelBuilder.Entity<UseCardChannelChild>(entity =>
            {
                entity.HasKey(e => e.Uuid)
                    .HasName("PK__useCardC__65A475E750B60148_copy1");

                entity.ToTable("useCardChannel_child");

                entity.Property(e => e.Uuid)
                    .HasColumnName("UUID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ChildChannelCode)
                    .HasColumnName("childChannelCode ")
                    .HasMaxLength(255);

                entity.Property(e => e.ChildChannelName)
                    .HasColumnName("childChannelName")
                    .HasMaxLength(255);

                entity.Property(e => e.ParentChannelCode)
                    .HasColumnName("parentChannelCode ")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<UseCardChannelParent>(entity =>
            {
                entity.HasKey(e => e.Uuid)
                    .HasName("PK__useCardC__65A475E750B60148");

                entity.ToTable("useCardChannel_parent");

                entity.Property(e => e.Uuid)
                    .HasColumnName("UUID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ParentsChannelCode)
                    .HasColumnName("parentsChannelCode")
                    .HasMaxLength(255);

                entity.Property(e => e.ParentsChannelName)
                    .HasColumnName("parentsChannelName")
                    .HasMaxLength(255);
            });
        }
    }
}
