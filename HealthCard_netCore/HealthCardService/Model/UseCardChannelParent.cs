﻿using System;
using System.Collections.Generic;

namespace HealthCardService.Model
{
    public partial class UseCardChannelParent
    {
        public string ParentsChannelCode { get; set; }
        public string ParentsChannelName { get; set; }
        public Guid Uuid { get; set; }
    }
}
