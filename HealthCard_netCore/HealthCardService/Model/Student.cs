﻿using System;
using System.Collections.Generic;

namespace HealthCardService.Model
{
    public partial class Student
    {
        public int Id { get; set; }
        public string StudentName { get; set; }
        public int? ClassInfoId { get; set; }

        public virtual ClassInfo ClassInfo { get; set; }
    }
}
