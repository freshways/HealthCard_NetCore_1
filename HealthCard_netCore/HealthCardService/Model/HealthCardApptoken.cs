﻿using System;
using System.Collections.Generic;

namespace HealthCardService.Model
{
    public partial class HealthCardApptoken
    {
        public int Id { get; set; }
        public string AppToken { get; set; }
        public long? ExpiresIn { get; set; }
        public string AppId { get; set; }
        public string AppSecret { get; set; }
    }
}
