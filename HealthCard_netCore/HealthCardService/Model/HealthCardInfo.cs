﻿using System;
using System.Collections.Generic;

namespace HealthCardService.Model
{
    public partial class HealthCardInfo
    {
        public string Name { get; set; }
        public string Gender { get; set; }
        public string Nation { get; set; }
        public string Birthday { get; set; }
        public string IdNumber { get; set; }
        public string Address { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string QrCodeText { get; set; }
        public string OpenId { get; set; }
        public string IdType { get; set; }
        public string Phid { get; set; }
        public string Patid { get; set; }
        public string HealthCardId { get; set; }
        public int Id { get; set; }
    }
}
