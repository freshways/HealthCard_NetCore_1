﻿using System;
using System.Collections.Generic;

namespace HealthCardService.Model
{
    public partial class HealthCardTemp
    {
        public string Name { get; set; }
        public string IdNumber { get; set; }
        public string Nation { get; set; }
        public string OpenId { get; set; }
        public int Id { get; set; }
        public string WechatCode { get; set; }
        public string HealthCode { get; set; }
    }
}
