﻿using System;
using System.Collections.Generic;

namespace HealthCardService.Model
{
    public partial class HealthCardHospitalList
    {
        public int Id { get; set; }
        public long? HospitalId { get; set; }
        public int? AppTokenId { get; set; }
    }
}
