﻿using System;
using System.Collections.Generic;

namespace HealthCardService.Model
{
    public partial class GobalResCode
    {
        public string ResCode { get; set; }
        public string ResMsg { get; set; }
        public string ResDetail { get; set; }
        public Guid Uuid { get; set; }
    }
}
