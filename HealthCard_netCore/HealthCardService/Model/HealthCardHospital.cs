﻿using System;
using System.Collections.Generic;

namespace HealthCardService.Model
{
    public partial class HealthCardHospital
    {
        public long Id { get; set; }
        public string HealthCardId { get; set; }
        public string HospitalNumber { get; set; }
        public string HospitalCode { get; set; }
    }
}
