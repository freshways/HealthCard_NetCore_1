﻿using System;
using System.Collections.Generic;

namespace swagger.Models
{
    public partial class HealthCardApptoken
    {
        public int Id { get; set; }
        public string AppToken { get; set; }
        public long? ExpiresIn { get; set; }
    }
}
