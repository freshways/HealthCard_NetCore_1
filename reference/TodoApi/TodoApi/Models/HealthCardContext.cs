﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace swagger.Models
{
    public partial class HealthCardContext : DbContext
    {
        public HealthCardContext()
        {
        }

        public HealthCardContext(DbContextOptions<HealthCardContext> options)
            : base(options)
        {
        }

        public virtual DbSet<HealthCardApptoken> HealthCardApptoken { get; set; }
        public virtual DbSet<HealthCardHospital> HealthCardHospital { get; set; }
        public virtual DbSet<HealthCardInfo> HealthCardInfo { get; set; }
        public virtual DbSet<HealthCardTemp> HealthCardTemp { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=192.168.10.152;Database=HealthCard;uid=sa;pwd=jkgly@163.com@152");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<HealthCardApptoken>(entity =>
            {
                entity.ToTable("healthCard_apptoken");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.AppToken)
                    .HasColumnName("appToken")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ExpiresIn).HasColumnName("expiresIn");
            });

            modelBuilder.Entity<HealthCardHospital>(entity =>
            {
                entity.ToTable("healthCard_hospital");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.HealthCardId)
                    .HasColumnName("healthCardId")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.HospitalCode)
                    .HasColumnName("hospitalCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HospitalNumber)
                    .HasColumnName("hospitalNumber")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<HealthCardInfo>(entity =>
            {
                entity.ToTable("healthCard_info");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Address)
                    .HasColumnName("address")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Birthday)
                    .HasColumnName("birthday")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .HasColumnName("gender")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.HealthCardId)
                    .HasColumnName("healthCardId")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IdNumber)
                    .HasColumnName("idNumber")
                    .HasMaxLength(18)
                    .IsUnicode(false);

                entity.Property(e => e.IdType)
                    .HasColumnName("idType")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nation)
                    .HasColumnName("nation")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OpenId)
                    .HasColumnName("openId")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Patid)
                    .HasColumnName("patid")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phid)
                    .HasColumnName("phid")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone1)
                    .HasColumnName("phone1")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Phone2)
                    .HasColumnName("phone2")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.QrCodeText)
                    .HasColumnName("qrCodeText")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<HealthCardTemp>(entity =>
            {
                entity.HasKey(e => e.OpenId)
                    .HasName("PK__HealthCa__72E12F1A7849DB76");

                entity.ToTable("HealthCard_temp");

                entity.Property(e => e.OpenId)
                    .HasColumnName("openId")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.HealthCode)
                    .HasColumnName("healthCode")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdNumber)
                    .HasColumnName("idNumber")
                    .HasMaxLength(18)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nation)
                    .HasColumnName("nation")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WechatCode)
                    .HasColumnName("wechatCode")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
        }
    }
}
