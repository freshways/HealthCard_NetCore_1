﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HealthCard.Helper;
using Microsoft.AspNetCore.Mvc;
using NLog;
using swagger.Models;

namespace TodoApi.Controllers
{
    [Produces("application/json")]
    [Route("api/jkgly")]
    //[Route("api/[controller]")]
    //[Route("api/[controller]/[action]")]
    [ApiController]
    public class jkglyController : ControllerBase
    {
        HealthCardContext healthCardContext = new HealthCardContext();
        private readonly ILogger iLogger = LogManager.GetCurrentClassLogger();

        //GET: api/jkgly/test
        //[HttpGet("test")]  自定义路由   
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            //var persones=await healthCardContext.HealthCardInfo.asy
            return Ok(healthCardContext.HealthCardInfo);
        }


        [HttpGet("{qrcodetext}")]
        public async Task<IActionResult> GetPersonAsync(string qrcodetext)
        {
            try
            {
                throw new Exception("A test except");
            }
            catch (Exception ex)
            {
                return StatusCode(500, "An Error Occurred");
            }
            //var person = healthCardContext.HealthCardInfo.Where(c => c.QrCodeText == qrcodetext).FirstOrDefault();
            //if (person == null)
            //{
            //    return NotFound();
            //}
            //return Ok(person);
        }
        //// GET: api/SysUsers/5
        //[HttpGet("{id}")]
        //public async Task<IActionResult> GetSysUser([FromRoute] int id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var sysUser = await _context.SysUsers.SingleOrDefaultAsync(m => m.Id == id);

        //    if (sysUser == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(sysUser);
        //}





        [HttpGet("getHeathCard")]
        public ActionResult<string> getHeathCard()
        {
            var personInfo = healthCardContext.HealthCardInfo.ToList().FirstOrDefault();
            return JsonHelper.SerializeJSON(personInfo);
            //iLogger.Info("测试logstash日志请求1");
            //iLogger.Warn("测试logstash日志请求警告1");
            //return new string[] { "value1", "value2" };
        }





        ///// <summary>
        ///// 这是一个带参数的get请求
        ///// </summary>
        //[HttpGet]
        //public ActionResult<string> Get()
        //{
        //    var personInfo = healthCardContext.HealthCardInfo.ToList().FirstOrDefault();
        //    return JsonHelper.SerializeJSON(personInfo);
        //    //iLogger.Info("测试logstash日志请求1");
        //    //iLogger.Warn("测试logstash日志请求警告1");
        //    //return new string[] { "value1", "value2" };
        //}





        //}

    }
}
