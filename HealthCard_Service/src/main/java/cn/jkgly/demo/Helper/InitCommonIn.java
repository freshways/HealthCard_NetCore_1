package cn.jkgly.demo.Helper;

import cn.jkgly.demo.service.HealthCardService;
import com.alibaba.fastjson.JSONObject;
import com.tencent.healthcard.impl.HealthCardServerImpl;
import com.tencent.healthcard.model.CommonIn;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.UUID;

public class InitCommonIn {
    @Autowired
    HealthCardService healthCardService;


    static String appSecret = "ebe203ce75d5b840c656370ca9a05e08";

    //构造公共输入参数commonIn
    static String appToken = "";
    static String requestId = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
    static String hospitalId = "30135";

    //构造请求参数req
   public static String appId = "609fe81c914cf2e7b6b8e273be9462ab";
    //调用接口
//    string appToken2="";

    public static CommonIn getCommonIn(String appToken) {
        CommonIn commonIn2 = new CommonIn(appToken, requestId, hospitalId);
        return commonIn2;
    }
    public static HealthCardServerImpl getHealthCard() {
        HealthCardServerImpl healthCard = new HealthCardServerImpl(appSecret);
        return healthCard;
    }
    public static void  main(String[] args) {
        /**
         * 接口：调用获取接口调用凭证appToken接口
         */
        //appSecret
        String appSecret="ebe203ce75d5b840c656370ca9a05e08";
        HealthCardServerImpl healthCard=new HealthCardServerImpl(appSecret);
        //构造公共输入参数commonIn
        String appToken="";
        String requestId= UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        String hospitalId="30135";
        CommonIn commonIn=new CommonIn(appToken,requestId,hospitalId);
        //构造请求参数req
        String appId="609fe81c914cf2e7b6b8e273be9462ab";
        //调用接口
        JSONObject appTokenObj=healthCard.getAppToken(commonIn,appId);
        //打印响应
        System.out.println("appToken:"+appTokenObj.toJSONString());
    }
    public static String getApptoken(){
        HealthCardServerImpl healthCard = new HealthCardServerImpl(appSecret);
        CommonIn commonIn = new CommonIn(appToken, requestId, hospitalId);
        JSONObject appTokenObj = healthCard.getAppToken(commonIn, appId);
        String apptoken1=appTokenObj.getJSONObject("rsp").getString("appToken");
        return apptoken1;
    }
}
