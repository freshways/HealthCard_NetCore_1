package cn.jkgly.demo.entity;

public class Image {
    private String imageContent;

    public String getImageContent() {
        return imageContent;
    }

    public void setImageContent(String imageContent) {
        this.imageContent = imageContent;
    }
}
