package cn.jkgly.demo.service;

import cn.jkgly.demo.entity.HealthCardInfoEntity;
import com.tencent.healthcard.model.HealthCardInfo;

import java.util.List;
import java.util.Map;

public interface HealthCardService {
    /**
     * 插入电子健康卡个人基本信息
     * @return
     */

    int  insertHealthCardInfo(HealthCardInfo healthCardInfo);

    /**
     * 根据openId查询健康卡信息
     * @param openId
     * @return
     */

    List<HealthCardInfo> findHealthCardInfoByOpenId(String openId);

    /**
     * 跟新wechatCode
     * @param wechatCode
     * @param openId
     * @return
     */

    int updateHealthCardInfoWechatCode(String wechatCode,String openId);
    /**
     * 跟新HealthCode
     * @param healthCode
     * @param openId
     * @return
     */

    int updateHealthCardInfoHealthCode(String healthCode,String openId);

    /**
     * 插入apptoekn
     * @param apptoken
     * @param expiresIn
     * @return
     */

    int insertHealthCardApptoken(String apptoken,Long expiresIn);

    /**
     * 查询apptoken
     * @return
     */

    String findHealthCardApptoken();
    /**
     * 查询ExpiresIn
     * @return
     */
    Long  findHealthCardExpiresIn();

    /**
     * 更新apptoken
     * @return
     */

    int updateHealthCardApptoken(String apptoken,Long expiresIn);

    /**
     * 添加临时信息
     * @param healthCardInfo
     * @return
     */
    int insertHealthCardTemp(HealthCardInfo healthCardInfo);

    /**
     * 跟新临时信息
     * @param healthCardInfo
     * @return
     */
    int updateHealthCardTemp(HealthCardInfo healthCardInfo);

    /**
     * 查询临时信息
     * @param openId
     * @return
     */
    HealthCardInfo findHealthCardTempByOpenId(String openId);
    /**
     *wechatCode
     * @param wechatCode
     * @param openId
     * @return
     */
    int insertHealthCardInfoWechatCode(String wechatCode,String openId);
    /**
     *添加healthCode
     * @param healthCode
     * @param openId
     * @return
     */
    int insertHealthCardInfoHealthCode(String healthCode,String openId);
    /**
     * 根据idNumber查询健康卡是否已注册
     * @param idNumber
     * @return
     */

    Integer findHealthCardInfoByIdNumber(String idNumber,String openId);
   /* *//**
     * 获取accseeToken
     *//*
    String getToken();

    *//**
     *更新accessToken
     * @param newToken
     * @param oldToken
     * @return
     *//*
    int upfateToken(String newToken,String oldToken);*/
}
