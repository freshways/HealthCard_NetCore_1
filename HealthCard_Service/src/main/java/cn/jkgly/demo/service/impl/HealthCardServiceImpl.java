package cn.jkgly.demo.service.impl;

import cn.jkgly.demo.entity.HealthCardInfoEntity;
import cn.jkgly.demo.jkglyDao.JkglyDao;
import cn.jkgly.demo.pubDao.HealthCardDao;
import cn.jkgly.demo.service.HealthCardService;
import com.tencent.healthcard.model.HealthCardInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class HealthCardServiceImpl implements HealthCardService {
    @Autowired
    HealthCardDao healthCardDao;
   /* @Autowired
    JkglyDao jkglyDao;*/
    @Override
    public int insertHealthCardInfo(HealthCardInfo healthCardInfo) {
        return healthCardDao.insertHealthCardInfo( healthCardInfo);
    }

    @Override
    public List<HealthCardInfo> findHealthCardInfoByOpenId(String openId) {
        return healthCardDao.findHealthCardInfoByOpenId(openId);
    }

    @Override
    public int updateHealthCardInfoWechatCode(String wechatCode, String openId) {
        return healthCardDao.updateHealthCardInfoWechatCode(wechatCode,openId);
    }

    @Override
    public int updateHealthCardInfoHealthCode(String healthCode, String openId) {
        return healthCardDao.updateHealthCardInfoHealthCode(healthCode,openId);
    }

    @Override
    public int insertHealthCardApptoken(String apptoken, Long expiresIn) {
        return healthCardDao.insertHealthCardApptoken(apptoken,expiresIn);
    }

    @Override
    public String findHealthCardApptoken() {
        return healthCardDao.findHealthCardApptoken();
    }

    @Override
    public Long findHealthCardExpiresIn() {
        Long l=healthCardDao.findHealthCardExpiresIn();
        System.out.println(l);
        return l;
    }

    @Override

    public int updateHealthCardApptoken(String apptoken,Long expiresIn) {
        return healthCardDao.updateHealthCardApptoken(apptoken,expiresIn);
    }

    @Override
    public int insertHealthCardTemp(HealthCardInfo healthCardInfo) {
        return healthCardDao.insertHealthCardTemp(healthCardInfo);
    }

    @Override
    public int updateHealthCardTemp(HealthCardInfo healthCardInfo) {
        return healthCardDao.updateHealthCardTemp(healthCardInfo);
    }

    @Override
    public HealthCardInfo findHealthCardTempByOpenId(String openId) {
        return healthCardDao.findHealthCardTempByOpenId(openId);
    }

    @Override
    public int insertHealthCardInfoWechatCode(String wechatCode, String openId) {
        return healthCardDao.insertHealthCardInfoWechatCode(wechatCode,openId);
    }

    @Override
    public int insertHealthCardInfoHealthCode(String healthCode, String openId) {
        return healthCardDao.insertHealthCardInfoHealthCode(healthCode,openId);
    }

    @Override
    public Integer findHealthCardInfoByIdNumber(String idNumber,String openId) {
        return healthCardDao.findHealthCardInfoByIdNumber(idNumber,openId);
    }

   /* @Override
    public String getToken() {
        return jkglyDao.getToken();
    }

    @Override
    public int upfateToken(String newToken, String oldToken) {
        return jkglyDao.upfateToken(newToken,oldToken);
    }*/
}
