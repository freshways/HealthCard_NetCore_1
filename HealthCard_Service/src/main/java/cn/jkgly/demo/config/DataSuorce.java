package cn.jkgly.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
@Configuration
public class DataSuorce {
    @Bean(name = "pub")
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource.pub") // application.properteis中对应属性的前缀
    public DataSource dataSource2() {
        return DataSourceBuilder.create().build();
    }
    /*@Bean(name = "jkgly")
    @ConfigurationProperties(prefix = "spring.datasource.jkgly") // application.properteis中对应属性的前缀
    public DataSource dataSource3() {
        return DataSourceBuilder.create().build();
    }*/
}
