package cn.jkgly.demo.pubDao;

import cn.jkgly.demo.entity.HealthCardInfoEntity;
import com.tencent.healthcard.model.HealthCardInfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

public interface HealthCardDao {
    /**
     * 插入电子健康卡个人基本信息
     * @return
     */
    @Insert("INSERT INTO healthCard_info ("
            + "name,\n"
            + "gender,\n"
            + "nation,\n"
            + "birthday,\n"
            + "idNumber,\n"
            + "address,\n"
            + "phone1,\n"
            + "phone2,\n"
            + "qrCodeText,\n"
            + "openId,\n"
            + "idType,\n"
            + "phid,\n"
            + "patid,\n"
            + "healthCardId )" +
            "values" +
            "(#{name},\n"
            + "#{gender},\n"
            + "#{nation},\n"
            + "#{birthday},\n"
            + "#{idNumber},\n"
            + "#{address},\n"
            + "#{phone1},\n"
            + "#{phone2},\n"
            + "#{qrCodeText},\n"
            + "#{openId},\n"
            + "#{idType},\n"
            + "#{phid},\n"
            + "#{patid},\n"
            + "#{healthCardId})")
    int  insertHealthCardInfo(HealthCardInfo healthCardInfo);

    /**
     * 根据openId查询健康卡信息
     * @param openId
     * @return
     */
    @Select("SELECT name,\n"
            + "gender,\n"
            + "nation,\n"
            + "birthday,\n"
            + "idNumber,\n"
            + "address,\n"
            + "phone1,\n"
            + "phone2,\n"
            + "qrCodeText,\n"
            + "openId,\n"
            + "idType,\n"
            + "phid,\n"
            + "patid,\n"
            + "healthCardId\n"
            + " FROM healthCard_info WHERE openId=#{openId}")
   List<HealthCardInfo> findHealthCardInfoByOpenId(@Param("openId")String openId);
   /**
    * 根据idNumber查询健康卡是否已注册
    * @param idNumber
    * @return
    */
   @Select("SELECT ISNULL(count(1) , 0) "
           + " FROM healthCard_info WHERE idNumber=#{idNumber} AND openId=#{openId}")
   Integer findHealthCardInfoByIdNumber(@Param("idNumber")String idNumber,@Param("openId")String openId);

    /**
     * 更新wechatCode
     * @param wechatCode
     * @param openId
     * @return
     */
    @Update("UPDATE healthCard_temp " +
            "SET wechatCode=#{wechatCode} " +
            "WHERE openId=#{openId}")
    int updateHealthCardInfoWechatCode(@Param("wechatCode")String wechatCode,@Param("openId")String openId);
    /**
     *添加wechatCode
     * @param wechatCode
     * @param openId
     * @return
     */
    @Insert("INSERT INTO  healthCard_temp " +
            "( wechatCode,openId) values" +
            "(#{wechatCode},#{openId}) " )
    int insertHealthCardInfoWechatCode(@Param("wechatCode")String wechatCode,@Param("openId")String openId);

    /**
     * 更新HealthCode
     * @param healthCode
     * @param openId
     * @return
     */
    @Update("UPDATE healthCard_temp " +
            "SET healthCode=#{healthCode} " +
            "WHERE openId=#{openId}")
    int updateHealthCardInfoHealthCode(@Param("healthCode")String healthCode,@Param("openId")String openId);

    /**
     *添加healthCode
     * @param healthCode
     * @param openId
     * @return
     */
    @Insert("INSERT INTO  healthCard_temp " +
            "( healthCode,openId) values" +
        "(#{healthCode},#{openId}) " )
    int insertHealthCardInfoHealthCode(@Param("healthCode")String healthCode,@Param("openId")String openId);

    /**
     * 插入apptoekn
     * @param apptoken
     * @param expiresIn
     * @return
     */
    @Insert("INSERT INTO healthCard_apptoken ( apptoken, expiresIn ) values" +
            "(#{apptoken}, #{expiresIn}) ")
    int insertHealthCardApptoken(@Param("apptoken")String apptoken,@Param("expiresIn") Long expiresIn);

    /**
     * 查询apptoken
     * @return
     */
    @Select("SELECT apptoken  FROM healthCard_apptoken where id=1")
    String  findHealthCardApptoken();
    /**
     * 查询expiresIn
     * @return
     */
    @Select("SELECT expiresIn  FROM healthCard_apptoken where id=1")
    Long  findHealthCardExpiresIn();

    /**
     * 更新apptoken
     * @return
     */
    @Update("UPDATE healthCard_apptoken " +
            "SET apptoken=#{apptoken}," +
            "expiresIn=#{expiresIn} " +
            "where id=1")
    int updateHealthCardApptoken(@Param("apptoken")String apptoken,@Param("expiresIn")Long expiresIn);

    /**
     * 添加临时信息
     * @param healthCardInfo
     * @return
     */
    @Insert("INSERT INTO healthCard_temp (name, idNumber, nation,openId) values (#{name},#{idNumber},#{nation},#{openId})")
    int insertHealthCardTemp(HealthCardInfo healthCardInfo);

    /**
     * 跟新临时信息
     * @param healthCardInfo
     * @return
     */
    @Update("UPDATE healthCard_temp " +
            "SET name=#{name}," +
            "idNumber=#{idNumber}," +
            "nation=#{nation} " +
            "WHERE openId=#{openId}")
    int updateHealthCardTemp(HealthCardInfo healthCardInfo);

    /**
     * 查询临时信息
     * @param openId
     * @return
     */
    @Select("SELECT * FROM healthCard_temp WHERE openId=#{openId}")

    HealthCardInfo findHealthCardTempByOpenId(@Param("openId")String openId);
}
