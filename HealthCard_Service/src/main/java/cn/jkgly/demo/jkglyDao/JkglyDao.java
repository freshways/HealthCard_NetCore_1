package cn.jkgly.demo.jkglyDao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface JkglyDao {
    @Select("SELECT ACCOUNTACCESSTOKEN FROM WEIXIN_ACCOUNT ")
    String getToken();
    @Update("UPDATE WEIXIN_ACCOUNT SET ACCOUNTACCESSTOKEN=#{newToken} WHERE ACCOUNTACCESSTOKEN=#{oldToken}) ")
    int upfateToken(@Param("newToken")String newToken,@Param("oldToken")String oldToken);
}
