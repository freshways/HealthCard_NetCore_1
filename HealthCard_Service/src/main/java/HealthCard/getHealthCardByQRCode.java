package HealthCard;

import HealthCard.Helper.InitCommonIn;
import HealthCard.Helper.InitHealthCardServerImpl;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tencent.healthcard.impl.HealthCardServerImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/article")
public class getHealthCardByQRCode {

    public String getHealthCardByQRCode(String[] args) {

        HealthCardServerImpl test = InitHealthCardServerImpl.getHealthCard();

        String healthCardId="71BDD379202D8BB2756E8F00F50017B3C4205DD843B83198EC1901675DDD8CA8";
        String idNumber="371323198104121416";
        String idType="01";
        //调用接口
        JSONObject dynamicQRCode=test.getDynamicQRCode(InitCommonIn.getCommonIn(),healthCardId,idType,idNumber);
        //打印响应
        System.out.println(JSON.toJSONString(dynamicQRCode));


//
//        String qrCodeText = "71BDD379202D8BB2756E8F00F50017B3C4205DD843B83198EC1901675DDD8CA8:1";
//
//        JSONObject healthCardInfoRsp = test.getHealthCardByQRCode(InitCommonIn.getCommonIn(), qrCodeText);
//        //打印响应
//        System.out.println(JSON.toJSONString(healthCardInfoRsp));
    }


}