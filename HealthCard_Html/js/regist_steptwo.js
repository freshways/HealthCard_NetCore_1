(function () {
    if (!auth()) return

    checkInput('.step-two')
    $(".form input").on("change", function (e) {
        checkInput('.step-two')
    })

    // 获取验证码
/*  $(".valid-btn").on("click", function(e) {
        var phone = $(".step-two .phone").val()
        $("#validnumber").val('1234')
        checkInput('.step-two')
         if (phone) {
            $.ajax({
                url: 'https://healthcarddemo.tengmed.com/index.php?c=healthcard&a=getSMSCode',
                data: {
                    phone: phone
                },
                dataType: 'json',
                type: 'post',
                success: function (res) {
                    if (res.retcode === 0) {
                        $("#validnumber").val(res.smsCode)
                        checkInput('.step-two')
                    } else {
                        alert(res.retmsg)
                    }
                }
            })
        } else {
            alert('请填写手机号码')
        } 
    })  */

    // 完成绑卡
    $(".next-btn").on('click', function () {
        var info = checkInput('.step-two')

        if (!isPoneAvailable($(".step-two .phone"))) {
            $("#phonefail-alert").addClass('show')
            return
        }


        if (info.status) {
            info.data.openId = localStorage.getItem('openID')
            $('.loading-mask').addClass('show')
            $.ajax({
                url: 'http://healthcard.jkgly.cn/api/jkgly/registerHealthCard',
                data: JSON.stringify(info.data),
                dataType: 'json',
                contentType: 'application/json',
                type: 'post',
                success: function (res) {
                    $('.loading-mask').removeClass('show')
                    if (res.retcode === 0) {
                        window.location.href = "list.html"
                    } else if (res.retcode === 302) {
                        window.location.href = res.redirect_uri
                    } else if (res.retcode === 10010 || res.retcode === 10011) {
                        var r = confirm(res.retmsg)
                        if (r == true) {
                            window.location.href = 'http://healthcard.jkgly.cn/api/jkgly/sendWechatCode?url=' + window.location.href.substr(7)+ '&openId=' + openID
                        }
                    } else if (res.retcode === 10015) {
                        var r = confirm(res.retmsg + '\r\n请确认身份信息填写是否正确。')
                        if (r == true) {
                            window.history.go(-1)
                        } else {
                            window.location.href = 'list.html'
                        }
                    } else {
                        alert(JSON.stringify(res))
                    }
                }
            })
        }
    })

})()

function checkInput(fa) {
    var phone = $(fa + " .phone").val()
    //var smsCode = $(fa + " .validnumber").val()
    if (phone) {                                 //&& smsCode) {
        $(fa + " .next-btn").removeAttr('disabled')
        return { status: true, data: { phone }}  //, smsCode } }
    } else {
        //$(fa + " .next-btn").attr('disabled', "disabled")
        return { status: false, data: { phone }}  //, smsCode } }
    }
}

function auth() {
    var login = getQueryString('login') || ''

    var wecode = getQueryString('code') || ''
	if(wecode === ''){
		wecode = localStorage.getItem('wecode') || ''
	}

    var openID = localStorage.getItem('openID') || ''
    
    if (wecode === '' && openID === '') {
        window.location.href = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx3a06c113295b44c3&redirect_uri=http%3a%2f%2f' + encodeURIComponent(window.location.href.substr(7)) + '&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect'  //'https://healthcarddemo.tengmed.com/index.php?c=healthcard&a=login'
	} else if (openID === '') {
		localStorage.setItem("wecode",wecode)
		//获取OpenID，暂存。
		$.ajax({
			url: 'http://healthcard.jkgly.cn/api/jkgly/getOpenId?code=' + wecode, 
			dataType: 'json',
			type: 'get',
			success: function (res) {
				if (res.openid) {
					localStorage.setItem('openID', res.openid),
                    openID = res.openid
				} else if (res.errcode) {
					alert(res.errmsg)
				}
				//清理wecode，防止过期出错。
				localStorage.removeItem('wecode')
			},
			error: function (res){
				alert(res)
			}
		})
	} else if (login === '') {
        window.location.href = 'http://healthcard.jkgly.cn/api/jkgly/sendWechatCode?url=' + window.location.href.substr(7) + '&openId=' + openID
        return false
    } else {
        return true
    }
}

//获取url参数
function getQueryString(name) {
  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); // 匹配目标参数
  var result = window.location.search.substr(1).match(reg); // 对querystring匹配目标参数
  if (result != null) {
    return decodeURIComponent(result[2]);
  } else {
    return null;
}
}

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=")
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1
            c_end = document.cookie.indexOf(";", c_start)
            if (c_end == -1) {
                c_end = document.cookie.length
            }
            return unescape(document.cookie.substring(c_start, c_end))
        }
    }
    return ""
}

//验证手机号码
function isPoneAvailable($poneInput) {
    var myreg=/^[1][3,4,5,7,8][0-9]{9}$/;
    if (!myreg.test($poneInput.val())) {
        return false;
    } else {
        return true;
    }
}

$("#confirm-btn").click(function() {
    $("#phonefail-alert").removeClass('show')
})
