(function () {
    if(!auth()) return

    $.ajax({
        url: 'http://healthcard.jkgly.cn/api/jkgly/getWXConfigSignature?url=' + encodeURIComponent(location.href.split('#')[0]),
        dataType: 'json',
        type: 'get',
        success: function (res) {
            if (res.appId != '') {
                wx.config({
                    debug: false,
                    appId: res.appId,
                    timestamp: res.timestamp,
                    nonceStr: res.nonceStr,
                    signature: res.signature,
                    jsApiList: ['config', 'chooseImage', 'uploadImage', 'getLocalImgData']
                })
            }
        }
    })

    wx.ready(function () {
        $(".upload-input").on("click", function (e) {
            e.preventDefault();
            wx.chooseImage({
                count: 1,
                sizeType: ['compressed'],
                sourceType: ['album', 'camera'],
                success: function (resp) {
                    $('.upload-image').attr('src', resp.localIds[0])
                    $('.loading-mask').addClass('show')
                    wx.getLocalImgData({
                        localId: resp.localIds[0],
                        success: function (res) {
                            var localData = res.localData   //.substr(localData.indexOf('base64,') + 7);
                            
                            //iOS、安卓兼容处理
                            if (localData.substr(0,4) === 'data') {
                                localData = localData.substr(localData.indexOf('base64,') + 7)
                            }

                            $('.loading-mask').addClass('show')
                            $.ajax({
                                url: 'http://healthcard.jkgly.cn/api/jkgly/registerByOcr',
                                data: JSON.stringify({ imageContent: localData }),
                                dataType: 'json',
                                type: 'post',
                                contentType: 'application/json',
                                success: function (res) {
                                    $('.loading-mask').removeClass('show')
                                    if (res.commonOut.resultCode === 0) {
                                        $('#after .name').val(res.rsp.cardInfo.name)
                                        $('#after .idcard').val(res.rsp.cardInfo.id)
                                        $('#after .nation').val(res.rsp.cardInfo.nation)
                                        $('#after .gender').val(res.rsp.cardInfo.gender)
                                        $('#after .address').val(res.rsp.cardInfo.address)
                                        checkInput('.photo-tab')
                                        $(".upload").addClass('uploaded')
                                    } else if (res.commonOut.resultCode === 302) {
                                        $("#ocrfail-alert").addClass('show')
                                    } else {
                                        // alert(res.retmsg)
                                        $("#ocrfail-alert").addClass('show')
                                    }
                                }
                            })
                        }
                    });
                }
            });
        })
    });

    $("#confirm-btn").click(function() {
        $("#ocrfail-alert").removeClass('show')
    })

    checkInput('.input-tab')
    $(".input-tab input").on("change", function (e) {
        checkInput('.input-tab')
    })
    $(".tab-element").on("click", function (e) {
        let attr = $(e.currentTarget).attr('data-tab')
        $(".tab-block").removeClass("input photo")
        $(".tab-block").addClass(attr)
    })

    var nations = [{ "value": "01", "text": "汉族" }, { "value": "02", "text": "蒙古族" }, { "value": "03", "text": "回族" }, { "value": "04", "text": "藏族" }, { "value": "05", "text": "维吾尔族" }, { "value": "06", "text": "苗族" }, { "value": "07", "text": "彝族" }, { "value": "08", "text": "壮族" }, { "value": "09", "text": "布依族" }, { "value": "10", "text": "朝鲜族" }, { "value": "11", "text": "满族" }, { "value": "12", "text": "侗族" }, { "value": "13", "text": "瑶族" }, { "value": "14", "text": "白族" }, { "value": "15", "text": "土家族" }, { "value": "16", "text": "哈尼族" }, { "value": "17", "text": "哈萨克族" }, { "value": "18", "text": "傣族" }, { "value": "19", "text": "黎族" }, { "value": "20", "text": "傈僳族" }, { "value": "21", "text": "佤族" }, { "value": "22", "text": "畲族" }, { "value": "23", "text": "高山族" }, { "value": "24", "text": "拉祜族" }, { "value": "25", "text": "水族" }, { "value": "26", "text": "东乡族" }, { "value": "27", "text": "纳西族" }, { "value": "28", "text": "景颇族" }, { "value": "29", "text": "柯尔克孜族" }, { "value": "30", "text": "土族" }, { "value": "31", "text": "达斡尔族" }, { "value": "32", "text": "仫佬族" }, { "value": "33", "text": "羌族" }, { "value": "34", "text": "布朗族" }, { "value": "35", "text": "撒拉族" }, { "value": "36", "text": "毛难族" }, { "value": "37", "text": "仡佬族" }, { "value": "38", "text": "锡伯族" }, { "value": "39", "text": "阿昌族" }, { "value": "40", "text": "普米族" }, { "value": "41", "text": "塔吉克族" }, { "value": "42", "text": "怒族" }, { "value": "43", "text": "乌孜别克族" }, { "value": "44", "text": "俄罗斯族" }, { "value": "45", "text": "鄂温克族" }, { "value": "46", "text": "崩龙族" }, { "value": "47", "text": "保安族" }, { "value": "48", "text": "裕固族" }, { "value": "49", "text": "京族" }, { "value": "50", "text": "塔塔尔族" }, { "value": "51", "text": "独龙族" }, { "value": "52", "text": "鄂伦春族" }, { "value": "53", "text": "赫哲族" }, { "value": "54", "text": "门巴族" }, { "value": "55", "text": "珞巴族" }, { "value": "56", "text": "基诺族" }]
    var picker = new Picker({
        data: [nations],
        selectedIndex: [0]
    });
    picker.on('picker.select', function (selectedVal, selectedIndex) {
        $("#nation").val(nations[selectedIndex[0]].text);
        $("#nation").trigger("change")
    })
    $(".select-nation").on("click", function (e) {
        picker.show();
    })

    $(".next-btn").on("click", function (e) {
        var tab = $(e.target).closest('.switch-tab')
        var info = checkInput('.' + tab[0].classList[1])
        if (info.status) {
            info.data.openId = localStorage.getItem('openID')
            $.ajax({
                url: 'http://healthcard.jkgly.cn/api/jkgly/saveCardInfo',
                data: JSON.stringify(info.data),
                dataType: 'json',
                type: 'post',
                contentType: 'application/json',
                success: function (res) {
                    if (res.retcode === 0) {
                        window.location.href = "regist_steptwo.html?login=1"
                    } else if (res.retcode === 302) {
                        window.location.href = res.redirect_uri
                    } else  {
                        alert(res.retmsg)
                    }
                }
            })
        }
    })


    //快速绑定健康卡
    $(".quick-bind").on("click", function (e) {
        window.location.href = 'http://healthcard.jkgly.cn/api/jkgly/sendRedirectHealthCard?url=healthcard.jkgly.cn/list.html&openId=' + localStorage.getItem('openID')
    })

})()

function checkInput(fa) {
    var name = $(fa+ " .name").val()
    var idNumber = $(fa+" .idcard").val()
    var nation = $(fa+" .nation").val()
    if (name && idNumber && nation) {
        $(fa+" .next-btn").removeAttr('disabled')
        return { status: true, data: {name, idNumber, nation }}
    } else {
        $(fa+" .next-btn").attr('disabled', "disabled")
        return { status: false, data: {name, idNumber, nation }}
    }
}

function auth() {
    var login = getQueryString('login') || ''

    var wecode = getQueryString('code') || ''
	if(wecode === ''){
		wecode = localStorage.getItem('wecode') || ''
	}

    var openID = localStorage.getItem('openID') || ''
    
    if (wecode === '' && openID === '') {
        window.location.href = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx3a06c113295b44c3&redirect_uri=http%3a%2f%2f' + encodeURIComponent(window.location.href.substr(7)) + '&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect'  //'https://healthcarddemo.tengmed.com/index.php?c=healthcard&a=login'
	} else if (openID === '') {
		localStorage.setItem("wecode",wecode)
		//获取OpenID，暂存。
		$.ajax({
			url: 'http://healthcard.jkgly.cn/api/jkgly/getOpenId?code=' + wecode, 
			dataType: 'json',
			type: 'get',
			success: function (res) {
				if (res.openid) {
					localStorage.setItem('openID', res.openid),
                    openID = res.openid
				} else if (res.errcode) {
					alert(res.errmsg)
				}
				//清理wecode，防止过期出错。
				localStorage.removeItem('wecode')
			},
			error: function (res){
				alert(res)
			}
		})
	} else if (login === '') {
        window.location.href = 'http://healthcard.jkgly.cn/api/jkgly/sendWechatCode?url=' + window.location.href.substr(7) + '&openId=' + openID
        return false
    } else {
        return true
    }
}

//获取url参数
function getQueryString(name) {
  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); // 匹配目标参数
  var result = window.location.search.substr(1).match(reg); // 对querystring匹配目标参数
  if (result != null) {
    return decodeURIComponent(result[2]);
  } else {
    return null;
  }
}

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=")
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1
            c_end = document.cookie.indexOf(";", c_start)
            if (c_end == -1) {
                c_end = document.cookie.length
            }
            return unescape(document.cookie.substring(c_start, c_end))
        }
    }
    return ""
}
