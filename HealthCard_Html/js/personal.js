(function () {
	init()
	$("#openpopwindow").click(function(){
		$(".mask").show()
	})
	$("#cancel-btn").click(function(){
		$(".mask").hide()
	})
	$("#unbing-btn").click(function () {
		$.ajax({
            url: 'http://healthcard.jkgly.cn/api/jkgly/deleteHealthCard',
            data: JSON.stringify({openId: getQueryString('openID'),qrCodeText: getQueryString('qrCodeText')}),
            dataType: 'json',
			type: 'post',
			contentType: 'application/json',
            success: function (res) {
                if(res.retcode === 0) {
                	window.location.href = 'list.html'
                } else {
                	alert(res.retmsg)
                }
            }
        })
	})
	$("#entercard").click(function () {
		window.location.href = 'http://healthcard.jkgly.cn/api/jkgly/takeMsCard?qrCodeText=' + getQueryString('qrCodeText')
	})
})()

function init () {
	var name = getQueryString('name') || ''
	var idCard = getQueryString('idCard') || '*****'
	var qrCodeText = getQueryString('qrCodeText') || ''
	var phone = getQueryString('phone') || ''

	$("#name").html(name)
	$("#idcard").html(idCard)
	$("#phone").html(phone)
	jQuery(".qrcode").qrcode({
		width : "200",               //二维码的宽度
        height : "200",
		render: "canvas",
		text: qrCodeText,
		background: "#ffffff",       //二维码的后景色
		foreground: "#000000",        //二维码的前景色
		src: 'img/logo_.png'             //二维码中间的图片
	}); 
}

function getQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); // 匹配目标参数
	var result = window.location.search.substr(1).match(reg); // 对querystring匹配目标参数
	if (result != null) {
	return decodeURIComponent(result[2]);
	} else {
	return null;
	}
}